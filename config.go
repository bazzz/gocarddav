package carddav

import (
	"gitlab.com/bazzz/carddav/data"
	"gitlab.com/bazzz/carddav/global"
)

func SetupStorage(stg data.Storage) {
	global.Storage = stg
}

/*
func SetupUser(username string) {
	global.User = &data.CalUser{username}
}
*/
