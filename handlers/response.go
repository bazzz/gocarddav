package handlers

import (
	"io"
	"net/http"

	"gitlab.com/bazzz/carddav/errs"
)

type Response struct {
	Status int
	Header http.Header
	Body   string
	Error  error
}

func NewResponse() *Response {
	return &Response{
		Header: make(http.Header),
	}
}

func (r *Response) Set(status int, body string) *Response {
	r.Status = status
	r.Body = body

	return r
}

func (r *Response) SetHeader(key, value string) *Response {
	r.Header.Set(key, value)

	return r
}

func (r *Response) SetError(err error) *Response {
	r.Error = err

	switch err {
	case errs.ErrResourceNotFound:
		r.Status = http.StatusNotFound
	case errs.ErrUnauthorized:
		r.Status = http.StatusUnauthorized
	case errs.ErrForbidden:
		r.Status = http.StatusForbidden
	default:
		r.Status = http.StatusInternalServerError
	}

	return r
}

func (r *Response) Write(writer http.ResponseWriter) {
	if r.Error == errs.ErrUnauthorized {
		r.SetHeader("WWW-Authenticate", `Basic realm="Restricted"`)
	}

	for key, values := range r.Header {
		for _, value := range values {
			writer.Header().Set(key, value)
		}
	}

	writer.WriteHeader(r.Status)
	io.WriteString(writer, r.Body)
}
