module gitlab.com/bazzz/carddav

go 1.16

require (
	github.com/beevik/etree v1.1.0
	github.com/laurent22/ical-go v0.1.0
)