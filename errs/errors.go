package errs

import (
	"errors"
)

var (
	ErrResourceNotFound      = errors.New("carddav: resource not found")
	ErrResourceAlreadyExists = errors.New("carddav: resource already exists")
	ErrUnauthorized          = errors.New("carddav: unauthorized. credentials needed")
	ErrForbidden             = errors.New("carddav: forbidden operation")
)
